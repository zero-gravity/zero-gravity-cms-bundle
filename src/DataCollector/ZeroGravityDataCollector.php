<?php

declare(strict_types=1);

namespace ZeroGravity\CmsBundle\DataCollector;

use Symfony\Bundle\FrameworkBundle\DataCollector\AbstractDataCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\VarDumper\Cloner\Data;
use Throwable;
use ZeroGravity\Cms\Content\ContentRepository;
use ZeroGravity\Cms\Content\File;
use ZeroGravity\Cms\Content\ReadablePage;

final class ZeroGravityDataCollector extends AbstractDataCollector
{
    public function __construct(
        private readonly ContentRepository $contentRepository
    ) {
    }

    public function getName(): string
    {
        return 'zero-gravity-cms';
    }

    public function collectServingFile(File $file): void
    {
        $this->data['served_file'] = $file;
    }

    public function collectServingPage(ReadablePage $page, string $template): void
    {
        $this->data['served_page'] = $page;
        $this->data['served_template'] = $template;
    }

    public function collect(Request $request, Response $response, Throwable $exception = null): void
    {
        $this->collectFromRequest($request);
        $this->data['all_pages'] = $this->contentRepository->getAllPages();
    }

    public function getRequestedPage(): ?ReadablePage
    {
        return $this->data['requested_page'] ?? null;
    }

    public function getRequestedFile(): ?File
    {
        return $this->data['requested_file'] ?? null;
    }

    public function getServedPage(): ?ReadablePage
    {
        return $this->data['served_page'] ?? null;
    }

    public function getServedFile(): ?File
    {
        return $this->data['served_file'] ?? null;
    }

    public function getServedTemplate(): ?string
    {
        return $this->data['served_template'] ?? null;
    }

    public function pageTaxonomies(ReadablePage $page): Data
    {
        return $this->cloneVar($page->getTaxonomies());
    }

    public function pageNonDefaultSettings(ReadablePage $page): Data
    {
        return $this->cloneVar($page->getNonDefaultSettings());
    }

    public function pageSettings(ReadablePage $page): Data
    {
        return $this->cloneVar($page->getSettings());
    }

    /**
     * @return array<string, ReadablePage>
     */
    public function allPages(): array
    {
        return $this->data['all_pages'] ?? [];
    }

    private function collectFromRequest(Request $request): void
    {
        $params = $request->attributes->get('_route_params');
        if (!is_array($params)) {
            return;
        }

        if (($params['_zg_page'] ?? null) instanceof ReadablePage) {
            $this->data['requested_page'] = $params['_zg_page'];
        }
        if (($params['_zg_file'] ?? null) instanceof File) {
            $this->data['requested_file'] = $params['_zg_file'];
        }
    }
}
