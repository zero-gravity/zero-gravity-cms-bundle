<?php

namespace ZeroGravity\CmsBundle;

use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use ZeroGravity\CmsBundle\DependencyInjection\CompilerPass\RegisterImagineLoaderPass;
use ZeroGravity\CmsBundle\DependencyInjection\CompilerPass\RegisterPageFiltersPass;
use ZeroGravity\CmsBundle\DependencyInjection\CompilerPass\RegisterTwigPathPass;

class ZeroGravityCmsBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(
            new RegisterImagineLoaderPass(),
            $type = PassConfig::TYPE_BEFORE_OPTIMIZATION,
            $priority = 1
        );
        $container->addCompilerPass(new RegisterTwigPathPass());
        $container->addCompilerPass(new RegisterPageFiltersPass());
    }
}
