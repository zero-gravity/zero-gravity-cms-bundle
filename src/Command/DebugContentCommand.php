<?php

namespace ZeroGravity\CmsBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ZeroGravity\Cms\Content\ContentRepository;

class DebugContentCommand extends Command
{
    public function __construct(
        private readonly ContentRepository $contentRepository
    ) {
        parent::__construct(null);
    }

    protected function configure(): void
    {
        $this
            ->setName('debug:zerogravity')
            ->addArgument('page', InputArgument::OPTIONAL, 'page path')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $page = $input->getArgument('page');
        if (is_string($page)) {
            dump($this->contentRepository->getPage($page));

            return Command::SUCCESS;
        }

        $pages = $this->contentRepository->getAllPages();
        dump($pages);
        dump(array_keys($pages));

        return Command::SUCCESS;
    }
}
