<?php

namespace ZeroGravity\CmsBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ZeroGravityCmsExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $mapper = $container->getDefinition('zero_gravity_cms.structure_mapper');
        $mapper->replaceArgument(1, $config['storage_path']);
        $mapper->replaceArgument(2, $config['convert_markdown']);
        $mapper->replaceArgument(3, $config['default_page_settings']);

        $fileFactory = $container->getDefinition('zero_gravity_cms.file_factory');
        $fileFactory->replaceArgument(2, $config['storage_path']);

        $routeProvider = $container->getDefinition('zero_gravity_cms.route_provider');
        $routeProvider->replaceArgument(1, $config['default_page_controller']);

        $container->setParameter('zero_gravity_cms.default_layout_template', $config['default_layout_template']);
        $container->setParameter('zero_gravity_cms.storage_path', $config['storage_path']);
    }
}
