<?php

namespace ZeroGravity\CmsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use ZeroGravity\CmsBundle\Controller\PageController;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('zero_gravity_cms');

        /* @noinspection NullPointerExceptionInspection */
        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('storage_path')
                    ->defaultValue('%kernel.project_dir%/pages')
                ->end()
                ->booleanNode('convert_markdown')
                    ->defaultTrue()
                ->end()
                ->scalarNode('default_layout_template')
                    ->defaultValue('@ZeroGravityCms/zg_page.html.twig')
                ->end()
                ->scalarNode('default_page_controller')
                    ->defaultValue(PageController::class.'::page')
                ->end()
                ->variableNode('default_page_settings')
                    ->treatNullLike([])
                    ->defaultValue([])
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
