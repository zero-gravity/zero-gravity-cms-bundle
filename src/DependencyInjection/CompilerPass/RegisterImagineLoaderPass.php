<?php

namespace ZeroGravity\CmsBundle\DependencyInjection\CompilerPass;

use Liip\ImagineBundle\DependencyInjection\Factory\Loader\FileSystemLoaderFactory;
use Liip\ImagineBundle\DependencyInjection\LiipImagineExtension;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Register an Imagine filesystem loader if the necessary prototype class exists.
 *
 * @see LiipImagineExtension
 */
class RegisterImagineLoaderPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition('liip_imagine.binary.loader.prototype.filesystem')) {
            // LiipImagineBundle is not enabled
            return;
        }
        if ($container->hasDefinition('liip_imagine.binary.loader.zero_gravity')) {
            // The loader has been defined elsewhere
            return;
        }

        $path = $container->getParameter('zero_gravity_cms.storage_path');

        $factory = new FileSystemLoaderFactory();
        $factory->create($container, 'zero_gravity', [
            'locator' => 'filesystem',
            'data_root' => [$path],
            'allow_unresolvable_data_roots' => false,
            'bundle_resources' => [
                'enabled' => false,
                'access_control_type' => 'blacklist',
                'access_control_list' => [],
            ],
        ]);
    }
}
