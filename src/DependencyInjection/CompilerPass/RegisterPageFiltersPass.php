<?php

namespace ZeroGravity\CmsBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class RegisterPageFiltersPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $registry = $container->getDefinition('zero_gravity_cms.filter_registry');

        foreach (array_keys($container->findTaggedServiceIds('zero_gravity.page_finder_filter')) as $serviceId) {
            $registry->addMethodCall('addFilter', [new Reference($serviceId)]);
        }
        foreach (array_keys($container->findTaggedServiceIds('zero_gravity.page_finder_filters')) as $serviceId) {
            $registry->addMethodCall('addFilters', [new Reference($serviceId)]);
        }
    }
}
