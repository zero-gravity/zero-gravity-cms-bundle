<?php

namespace ZeroGravity\CmsBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RegisterTwigPathPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $path = $container->getParameter('zero_gravity_cms.storage_path');

        if ($container->hasDefinition('twig.loader.native_filesystem')) {
            $loader = $container->getDefinition('twig.loader.native_filesystem');
        } elseif ($container->hasDefinition('twig.loader.filesystem')) {
            $loader = $container->getDefinition('twig.loader.filesystem');
        } else {
            return;
        }

        $loader->addMethodCall('addPath', [$path, 'ZeroGravity']);
    }
}
