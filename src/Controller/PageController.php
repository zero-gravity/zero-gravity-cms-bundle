<?php

namespace ZeroGravity\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use ZeroGravity\Cms\Content\File;
use ZeroGravity\Cms\Content\Page;
use ZeroGravity\CmsBundle\DataCollector\ZeroGravityDataCollector;

class PageController extends AbstractController
{
    public function __construct(
        private readonly string $defaultLayoutTemplate,
        private readonly ZeroGravityDataCollector $dataCollector,
    ) {
    }

    public function page(Page $_zg_page, File $_zg_file = null): Response
    {
        if ($_zg_file instanceof File) {
            $this->dataCollector->collectServingFile($_zg_file);

            return $this->file($_zg_file->getFilesystemPathname());
        }

        if (!$_zg_page->isPublished()) {
            throw $this->createNotFoundException();
        }

        $template = $_zg_page->getLayoutTemplate() ?? $this->defaultLayoutTemplate;
        $this->dataCollector->collectServingPage($_zg_page, $template);

        return $this->render($template, [
            'page' => $_zg_page,
        ]);
    }
}
