<?php

// https://github.com/squizlabs/PHP_CodeSniffer/issues/2015
// phpcs:disable PSR1.Files.SideEffects
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
// phpcs:disable Generic.Files.LineLength.TooLong
// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClass

define('C33S_SKIP_LOAD_DOT_ENV', true);
/*
 * =================================================================
 * Start CI auto fetch (downloading robo dependencies automatically)
 * =================================================================.
 */
define('C33S_ROBO_DIR', '.robo');

$roboDir = C33S_ROBO_DIR;
$previousWorkingDir = getcwd();
if (is_dir($roboDir) || mkdir($roboDir)) {
    chdir($roboDir);
}
if (!is_file('composer.json')) {
    exec('composer init --no-interaction', $output, $resultCode);
    exec('composer require c33s/robofile --no-interaction', $output, $resultCode);
    exec('rm composer.yaml || rm composer.yml || return true', $output, $resultCode2);
    if ($resultCode > 0) {
        copy('https://getcomposer.org/composer.phar', 'composer');
        exec('php composer require c33s/robofile --no-interaction');
        unlink('composer');
    }
} else {
    exec('composer install --dry-run --no-interaction 2>&1', $output);
    if (false === in_array('Nothing to install or update', $output)) {
        fwrite(STDERR, "\n##### Updating .robo dependencies #####\n\n") && exec('composer install --no-interaction');
    }
}
chdir($previousWorkingDir);
require $roboDir.'/vendor/autoload.php';

/*
 * =================================================================
 *                        End CI auto fetch
 * =================================================================.
 */

use C33s\Robo\BaseRoboFile;
use C33s\Robo\C33sExtraTasks;
use C33s\Robo\C33sTasks;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends BaseRoboFile
{
    use C33sTasks;
    use C33sExtraTasks;

    private const GLOBAL_COMPOSER_PACKAGES = [
    ];

    protected array $portsToCheck = [
        // 'http' => null,
        // 'https' => null,
        // 'mysql' => null,
        // 'postgres' => null,
        // 'elasticsearch' => null,
        // 'mongodb' => null,
    ];

    /**
     * @hook pre-command
     */
    public function preCommand(): void
    {
        $this->stopOnFail(true);
        $this->_prepareCiModules([
            'composer' => '2.6.6',
            'php-cs-fixer' => 'v3.48.0',
            'phpstan' => '1.10.56',
            'phpcs' => '3.7.2',
        ]);
    }

    /**
     * Initialize project.
     */
    public function init(): void
    {
        if (!$this->confirmIfInteractive('Have you read the README.md?')) {
            $this->abort();
        }

        if (!$this->ciCheckPorts($this->portsToCheck) && !$this->confirmIfInteractive('Do you want to continue?')) {
            $this->abort();
        }

        foreach (self::GLOBAL_COMPOSER_PACKAGES as $package => $version) {
            $this->composerGlobalRequire($package, $version);
        }

        $this->update();
    }

    /**
     * Perform code-style checks.
     *
     * @option       fix Run auto-fixer before running checks
     *
     * @noinspection PhpParameterNameChangedDuringInheritanceInspection
     */
    public function check(
        array $opts = [
            'fix' => false,
            'rector-args' => '',
            'php-cs-fixer-args' => '',
            'phpstan-args' => '',
        ],
    ): void {
        if ($opts['fix']) {
            $this->fix('', true);
        }

        $this->_execPhp("php ./vendor/bin/rector process --dry-run {$opts['rector-args']}");
        $this->_execPhp("php ./{$this->dir()}/bin/php-cs-fixer.phar fix --verbose --dry-run {$opts['php-cs-fixer-args']}");
        $this->_execPhp("php ./vendor/bin/phpstan analyse {$opts['phpstan-args']}");
    }

    /**
     * Perform code-style checks and cleanup source code automatically.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function fix(string $arguments = '', bool $force = false): void
    {
        if ($force || $this->confirmIfInteractive('Do you really want to run php-cs-fixer on your source code?')) {
            $this->_execPhp('php ./vendor/bin/rector process');
            $this->_execPhp("php ./{$this->dir()}/bin/php-cs-fixer.phar fix --verbose {$arguments}");
        } else {
            $this->abort();
        }
    }

    /**
     * Run tests.
     */
    public function test(): void
    {
        $this->_execPhp('php ./vendor/bin/codecept run --coverage-xml --coverage-html --coverage-text', true);
        $this->outputCoverage();
    }

    /**
     * Setup embedded demo project.
     */
    public function initDemoProject(): void
    {
        $this->_execPhp("php ./{$this->dir()}/bin/composer.phar install --no-progress --prefer-dist --optimize-autoloader --working-dir=./tests/_demo-project");
        $this
            ->taskExecPhp('php bin/console cache:warmup --env=test')
            ->dir('tests/_demo-project')
            ->run()
        ;
    }

    /**
     * Run tests.
     */
    public function testDemoProject(): void
    {
        $this
            ->taskExecPhp('php ./vendor/bin/codecept run')
            ->dir('tests/_demo-project')
            ->run()
        ;
    }

    /**
     * Write plain coverage line used for gitlab CI detecting the coverage score.
     */
    private function outputCoverage(): void
    {
        $this->writeln(file(__DIR__.'/tests/_output/coverage.txt')[8]);
    }

    /**
     * Update the Project.
     */
    public function update(): void
    {
        if ($this->isEnvironmentCi() || $this->isEnvironmentProduction()) {
            $this->_execPhp("php ./{$this->dir()}/bin/composer.phar install --no-progress --prefer-dist --optimize-autoloader");
        } else {
            $this->_execPhp("php ./{$this->dir()}/bin/composer.phar install");
        }
    }

    /**
     * Check if the current mode is interactive.
     */
    protected function isInteractive(): bool
    {
        return $this->input()->isInteractive() && !$this->isEnvironmentCi();
    }
}
