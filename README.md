ZeroGravity CMS Symfony Bundle
==============================

Integrate [ZeroGravity CMS](https://gitlab.com/zero-gravity/zero-gravity-cms) into your
Symfony 6.4+ project.

[![pipeline status](https://gitlab.com/zero-gravity/zero-gravity-cms-bundle/badges/master/pipeline.svg)](https://gitlab.com/zero-gravity/zero-gravity-cms-bundle/commits/master)
[![Coverage Status](https://coveralls.io/repos/gitlab/zero-gravity/zero-gravity-cms-bundle/badge.svg?branch=main)](https://coveralls.io/gitlab/zero-gravity/zero-gravity-cms-bundle?branch=main)
