<?php

declare(strict_types=1);

namespace Tests\Support\Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Codeception\Module;
use Codeception\Module\Asserts;
use Codeception\Module\Symfony;
use RuntimeException;

class Functional extends Module
{
    private function grabSymfony(): Symfony
    {
        $module = $this->getModule('Symfony');
        if (!$module instanceof Symfony) {
            throw new RuntimeException();
        }

        return $module;
    }

    private function grabAsserts(): Asserts
    {
        $module = $this->getModule('Asserts');
        if (!$module instanceof Asserts) {
            throw new RuntimeException();
        }

        return $module;
    }

    public function seeMenuWithCurrentItemAt(string $currentPath): void
    {
        $this->grabSymfony()->seeElement("#menu-container .current > a[href=\"$currentPath\"]");
    }

    public function dontSeeMenu(): void
    {
        $this->grabSymfony()->dontSeeElement("#menu-container");
    }
}
