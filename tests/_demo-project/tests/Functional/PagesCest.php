<?php

declare(strict_types=1);

namespace Tests\Functional;

use Codeception\Attribute\DataProvider;
use Codeception\Example;
use Tests\Support\FunctionalTester;

final class PagesCest
{
    #[DataProvider('_providePages')]
    public function pageLoads(FunctionalTester $I, Example $example): void
    {
        $I->amOnPage($example['path']);
        $I->seeResponseCodeIsSuccessful();

        if (false === $example['title']) {
            $I->dontSeeElement('h1');
        } else {
            $I->see($example['title'], 'h1');
        }

        if ($example['menu']) {
            $I->seeMenuWithCurrentItemAt($example['path']);
        } else {
            $I->dontSeeMenu();
        }

        foreach ((array) $example['text'] as $text) {
            $I->see($text);
        }
    }

    public static function _providePages(): iterable
    {
        yield 'home' => [
            'path' => '/',
            'menu' => true,
            'title' => 'Home page',
            'text' => [
                "This is the project's home page.",
                'Basic layout for all zero gravity pages',
            ],
        ];

        yield 'another-page' => [
            'path' => '/another-page',
            'menu' => true,
            'title' => 'Another Page',
            'text' => [
                "This page uses a custom twig template.",
                'Basic layout for all zero gravity pages',
            ],
        ];

        yield 'yet-another-page' => [
            'path' => '/yet-another-page',
            'menu' => true,
            'title' => 'Yet Another Page',
            'text' => [
                'Basic layout for all zero gravity pages',
            ],
        ];

        yield 'yet-another-page-sub-page1' => [
            'path' => '/yet-another-page/sub-page1',
            'menu' => true,
            'title' => 'Foo',
            'text' => [
                'foo',
                'Basic layout for all zero gravity pages',
            ],
        ];

        yield 'advanced-examples' => [
            'path' => '/advanced-examples',
            'menu' => false,
            'title' => false,
            'text' => [
                'This page uses a custom twig layout, without the default menu.',
                'PageFinder examples',
            ],
        ];
    }
}
