---
title: Home page
slug: ~
taxonomy:
    tag:
        - my-tag
---

This is the project's home page.

Supported features:

- Markdown
- Menus
