<?php

namespace Tests\Unit\ZeroGravity\CmsBundle\DependencyInjection\CompilerPass;

use Codeception\Test\Unit;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use ZeroGravity\Cms\Content\Finder\FilterRegistry;
use ZeroGravity\CmsBundle\DependencyInjection\CompilerPass\RegisterPageFiltersPass;

class RegisterPageFiltersPassTest extends Unit
{
    #[Test]
    public function singleFilterIsAddedForServiceTaggedAsSingleFilter(): void
    {
        $container = new ContainerBuilder();
        $container->setDefinition('zero_gravity_cms.filter_registry', new Definition(FilterRegistry::class));

        $filter = new Definition('');
        $filter->addTag('zero_gravity.page_finder_filter');

        $container->setDefinition('some_filter', $filter);

        $this->process($container);

        $calls = $container->getDefinition('zero_gravity_cms.filter_registry')->getMethodCalls();
        self::assertCount(1, $calls);
        self::assertSame('addFilter', $calls[0][0]);
    }

    #[Test]
    public function multiFilterIsAddedForServiceTaggedAsMultiFilter(): void
    {
        $container = new ContainerBuilder();
        $container->setDefinition('zero_gravity_cms.filter_registry', new Definition(FilterRegistry::class));

        $filter = new Definition('');
        $filter->addTag('zero_gravity.page_finder_filters');

        $container->setDefinition('some_filters', $filter);

        $this->process($container);

        $calls = $container->getDefinition('zero_gravity_cms.filter_registry')->getMethodCalls();
        self::assertCount(1, $calls);
        self::assertSame('addFilters', $calls[0][0]);
    }

    private function process(ContainerBuilder $container): void
    {
        $pass = new RegisterPageFiltersPass();
        $pass->process($container);
    }
}
