<?php

namespace Tests\Unit\ZeroGravity\CmsBundle\DependencyInjection\CompilerPass;

use Codeception\Test\Unit;
use Liip\ImagineBundle\Binary\Loader\FileSystemLoader;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use ZeroGravity\CmsBundle\DependencyInjection\CompilerPass\RegisterImagineLoaderPass;

class RegisterImagineLoaderPassTest extends Unit
{
    #[Test]
    public function noLoaderIsCreatedWhenImagineIsNotPresent(): void
    {
        $container = new ContainerBuilder();
        $this->process($container);

        self::assertFalse($container->hasDefinition('liip_imagine.binary.locator.filesystem.zero_gravity'));
        self::assertFalse($container->hasDefinition('liip_imagine.binary.loader.zero_gravity'));
    }

    #[Test]
    public function noLoaderIsCreatedWhenZeroGravityLoaderIsAlreadyPresent(): void
    {
        $container = new ContainerBuilder();
        $container->setDefinition('liip_imagine.binary.loader.prototype.filesystem', new Definition(FileSystemLoader::class));
        $container->setDefinition('liip_imagine.binary.loader.zero_gravity', new Definition(FileSystemLoader::class));
        $container->setParameter('zero_gravity_cms.storage_path', '');

        $this->process($container);

        self::assertFalse($container->hasDefinition('liip_imagine.binary.locator.filesystem.zero_gravity'));
        self::assertTrue($container->hasDefinition('liip_imagine.binary.loader.zero_gravity'));
    }

    #[Test]
    public function loaderIsCreatedWhenImagineIsPresent(): void
    {
        $container = new ContainerBuilder();
        $container->setDefinition('liip_imagine.binary.loader.prototype.filesystem', new Definition(FileSystemLoader::class));
        $container->setParameter('zero_gravity_cms.storage_path', '');

        $this->process($container);

        self::assertTrue($container->hasDefinition('liip_imagine.binary.loader.zero_gravity'));
    }

    private function process(ContainerBuilder $container): void
    {
        $pass = new RegisterImagineLoaderPass();
        $pass->process($container);
    }
}
