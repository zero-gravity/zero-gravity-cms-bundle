<?php

namespace Tests\Unit\ZeroGravity\CmsBundle\DependencyInjection\CompilerPass;

use Codeception\Test\Unit;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Twig\Loader\FilesystemLoader;
use ZeroGravity\CmsBundle\DependencyInjection\CompilerPass\RegisterTwigPathPass;

class RegisterTwigPathPassTest extends Unit
{
    #[Test]
    public function nothingHappensIfTwigFilesystemLoaderIsNotPresent(): void
    {
        $container = new ContainerBuilder();
        $container->setParameter('zero_gravity_cms.storage_path', '');

        $this->process($container);

        self::assertTrue(true);
    }

    #[Test]
    public function twigFilesystemLoaderReceivesCallToAddZeroGravityPath(): void
    {
        $container = new ContainerBuilder();
        $container->setDefinition('twig.loader.filesystem', new Definition(FilesystemLoader::class));
        $container->setParameter('zero_gravity_cms.storage_path', '');

        $this->process($container);

        $calls = $container->getDefinition('twig.loader.filesystem')->getMethodCalls();
        self::assertCount(1, $calls);
        self::assertSame('addPath', $calls[0][0]);
        self::assertSame('ZeroGravity', $calls[0][1][1]);
    }

    #[Test]
    public function twigNativeFilesystemLoaderReceivesCallToAddZeroGravityPath(): void
    {
        $container = new ContainerBuilder();
        $container->setDefinition('twig.loader.native_filesystem', new Definition(FilesystemLoader::class));
        $container->setParameter('zero_gravity_cms.storage_path', '');

        $this->process($container);

        $calls = $container->getDefinition('twig.loader.native_filesystem')->getMethodCalls();
        self::assertCount(1, $calls);
        self::assertSame('addPath', $calls[0][0]);
        self::assertSame('ZeroGravity', $calls[0][1][1]);
    }

    private function process(ContainerBuilder $container): void
    {
        $pass = new RegisterTwigPathPass();
        $pass->process($container);
    }
}
