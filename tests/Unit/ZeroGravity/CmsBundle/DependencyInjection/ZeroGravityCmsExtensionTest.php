<?php

namespace Tests\Unit\ZeroGravity\CmsBundle\DependencyInjection;

use Codeception\Test\Unit;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use ZeroGravity\Cms\Content\FileFactory;
use ZeroGravity\Cms\Filesystem\FilesystemMapper;
use ZeroGravity\Cms\Routing\RouteProvider;
use ZeroGravity\CmsBundle\DependencyInjection\ZeroGravityCmsExtension;

class ZeroGravityCmsExtensionTest extends Unit
{
    #[Test]
    public function extensionConfiguresStructureMapper(): void
    {
        $container = $this->getContainerBuilderWithLoadedExtension();

        $arguments = $container->getDefinition('zero_gravity_cms.structure_mapper')->getArguments();
        self::assertSame('some/path', $arguments[1]);
        self::assertFalse($arguments[2]);
        self::assertSame(['title' => 'test-title'], $arguments[3]);
    }

    #[Test]
    public function extensionConfiguresFileFactory(): void
    {
        $container = $this->getContainerBuilderWithLoadedExtension();

        $arguments = $container->getDefinition('zero_gravity_cms.file_factory')->getArguments();
        self::assertSame('some/path', $arguments[2]);
    }

    #[Test]
    public function extensionConfiguresRouteProvider(): void
    {
        $container = $this->getContainerBuilderWithLoadedExtension();

        $arguments = $container->getDefinition('zero_gravity_cms.route_provider')->getArguments();
        self::assertSame('some::controller', $arguments[1]);
    }

    #[Test]
    public function extensionConfiguresParameters(): void
    {
        $container = $this->getContainerBuilderWithLoadedExtension();

        self::assertTrue($container->hasParameter('zero_gravity_cms.default_layout_template'));
        self::assertSame('some/template.html.twig', $container->getParameter('zero_gravity_cms.default_layout_template'));
        self::assertTrue($container->hasParameter('zero_gravity_cms.storage_path'));
        self::assertSame('some/path', $container->getParameter('zero_gravity_cms.storage_path'));
    }

    private function getContainerBuilderWithLoadedExtension(): ContainerBuilder
    {
        $container = new ContainerBuilder();
        $container->setDefinition('zero_gravity_cms.structure_mapper', new Definition(FilesystemMapper::class));
        $container->setDefinition('zero_gravity_cms.file_factory', new Definition(FileFactory::class));
        $container->setDefinition('zero_gravity_cms.route_provider', new Definition(RouteProvider::class));

        $extension = new ZeroGravityCmsExtension();

        $configs = [
            [
                'default_layout_template' => 'some/template.html.twig',
                'default_page_controller' => 'some::controller',
                'storage_path' => 'some/path',
                'convert_markdown' => false,
                'default_page_settings' => ['title' => 'test-title'],
            ],
        ];
        $extension->load($configs, $container);

        return $container;
    }
}
